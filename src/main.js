import Vue from 'vue'
import App from './App.vue'
import firebase from 'firebase'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
firebase.initializeApp({
  apiKey: "XXXXXX",
  authDomain: "XXXXXXXX",
  databaseURL: "XXXXXX",
  projectId: "XXXXXXX",
  storageBucket: "XXXXXXX",
  messagingSenderId: "XXXXXX",
  appId: "XXXXXXX",
  measurementId: "XXXXX"
})